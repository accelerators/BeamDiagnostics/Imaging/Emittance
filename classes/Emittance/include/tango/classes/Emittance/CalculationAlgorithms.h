#ifndef CALCULATION_ALGORITHMS_H
#define CALCULATION_ALGORITHMS_H

#include <iostream>
#include <tango/classes/Emittance/Emittance.h>
#include <tango/tango.h>
#include <math.h>

// Different Emittance calculation algorithm and ...
typedef enum
{
    PinholeCamera = 0,
    Terminator
} EmittanceAlgorithmID;
 

namespace Emittance_ns
{

/*
 * This class defines interface for differents Emittance
 * calculation Algorithms.
 */
class EmittanceCalculationBase
{
public:
    EmittanceCalculationBase(Emittance *);
    virtual tuple<double,double,double> calculateXEmittance(double) = 0;
    virtual tuple<double,double,double> calculateZEmittance(double) = 0;
    virtual double calculateSigmaXSource(Tango::Attribute &attr);
    virtual double calculateSigmaZSource(Tango::Attribute &attr);
    virtual double calculateSigmaZSourceCorr(Tango::Attribute &attr);
    virtual ~EmittanceCalculationBase(){};

    const char *getName() {return calculationName;}

protected:

    const char *calculationName;
    Emittance *emittance;

    double sigmaXSource_um;
    double sigmaZSource_um;

private:

};


class PinholeCameraCalculation : public EmittanceCalculationBase
{
public:

    struct _BeamData
    {
        double  beta;
        double  disp;
        double  e_spread;
        double  sigma;
        double  emit;
    };

    typedef struct _BeamData BeamData;

    PinholeCameraCalculation(Emittance *);
    tuple<double,double,double> calculateXEmittance(double);
    tuple<double,double,double> calculateZEmittance(double);
    double calculateSigmaXSource(Tango::Attribute &attr);
    double calculateSigmaZSource(Tango::Attribute &attr);
    double calculateSigmaZSourceCorr(Tango::Attribute &attr);
    void computeError(BeamData &,double *,double *);

    double sigmaZSourceCorr_um;
};


} // Emittance_ns


#endif

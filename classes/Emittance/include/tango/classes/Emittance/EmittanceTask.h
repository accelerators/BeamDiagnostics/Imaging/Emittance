#ifndef EMITTANCE_TASK
#define EMITTANCE_TASK

#include <acu/tango/device/task.h>
#include <acu/threading/message.h>

#include <tango/tango.h>

#include <atomic>
#include <limits>

namespace Emittance_ns
{

struct EmittanceSetup
{
    //! Pinhole point spread function in um
    double pinholePSF = std::numeric_limits<double>::quiet_NaN();

    //! Pinhole point spread function in um
    double detectorPSF = std::numeric_limits<double>::quiet_NaN();

    //! Absolute error on pinhole psf
    double absErrPinholePSF = std::numeric_limits<double>::quiet_NaN();

    //! Absolute error on detector psf
    double absErrDetectorPSF = std::numeric_limits<double>::quiet_NaN();

    //! Asbolute error on beam size on screen
    double absErrScreen = std::numeric_limits<double>::quiet_NaN();

    //! Relative error on beta
    double relErrBeta = std::numeric_limits<double>::quiet_NaN();

    //! Relative error on magnification
    double relErrMagnification = std::numeric_limits<double>::quiet_NaN();

    //! Relative error on dispersion
    double relErrDispersion = std::numeric_limits<double>::quiet_NaN();

    //! Relative error on espread
    double relErrEspread = std::numeric_limits<double>::quiet_NaN();

    //! Distance from pinhole to screen
    double distPinholeScreen = std::numeric_limits<double>::quiet_NaN();

    //! Distance from source to pinhole
    double distSourcePinhole = std::numeric_limits<double>::quiet_NaN();

    //! Pixel size
    double pixelSize = std::numeric_limits<double>::quiet_NaN();

    //! Magnification
    double magnification = std::numeric_limits<double>::quiet_NaN();
};

struct Optics
{
    //! Horizontal beta function
    double xBeta = std::numeric_limits<double>::quiet_NaN();

    //! Vertical beta function
    double zBeta = std::numeric_limits<double>::quiet_NaN();

    //! Horizontal dispersion
    double xDispersion = std::numeric_limits<double>::quiet_NaN();

    //! Vertical dispersion
    double zDispersion = std::numeric_limits<double>::quiet_NaN();

    //! Energy spread
    double eSpread = std::numeric_limits<double>::quiet_NaN();

    //! True if the optics param are initialized
    bool initialized = false;
};

struct EmittanceValue
{
    //! Emittance value (in mm)
    double emittance = std::numeric_limits<double>::quiet_NaN();

    //! Sigma
    double sigma = std::numeric_limits<double>::quiet_NaN();

    //! Relative error
    double relativeError = std::numeric_limits<double>::quiet_NaN();

    //! Absolute error
    double absoluteError = std::numeric_limits<double>::quiet_NaN();
};

//! Task that produces the emittance value
/*!
 * This task has two main activities:
 *
 * First it produces a value for the emittance for each frame of the camera. The
 * task subscribes to a BPM CCD event that signals every new camera frame and
 * carries a new FWHM value of this specific frame. This emittance value is
 * published through an event and during injection, the task can be asked to
 * also archive this emittance value.
 *
 * It also produces periodically a more stable value for the emittance. It is
 * calculated based on the (averaged) FWHM extracted from BBP CCD. This
 * emittance is accessible through two getter function (vertical and horizontal
 * emittances).
 *
 * To be able to calculate the emittance, the task has to collect the optics.
 * This is done through the corresponding event subscription.
 */
class EmittanceTask: public acu::tango::dev::Task
{
public:
    //! Constructor
    /*!
     * \param device Emittance device that owns the task
     * \param bpmDevice CCD BPM device
     * \param opticsDevice Beam optics device
     * \param injectionDevice Device that publishes the injection event
     * \param eSetup Constant values of the optical setup
     */
    EmittanceTask(Tango::DeviceImpl *device, Tango::DeviceProxy &bpmDevice,
                  Tango::DeviceProxy &opticsDevice,
                  Tango::DeviceProxy &injectionDevice,
                  const EmittanceSetup &eSetup);

    //! Get the horizontal emittamce
    /*!
     * \return Horizontal emittance
     */
    EmittanceValue getHorizontalEmittance(void);

    //! Get the vertical emittance
    /*!
     * \return Vertical emittance
     */
    EmittanceValue getVerticalEmittance(void);

    //! Get the horizontal beta function
    /*!
     * \return Horizontal beta
     */
    double getHorizontalBeta(void);

    //! Get the vertical beta function
    /*!
     * \return Vertical beta
     */
    double getVerticalBeta(void);

    //! Get the horizontal dispersion
    /*!
     * \return Horizontal dispersion
     */
    double getHorizontalDispersion(void);

    //! Get the vertical dispersion
    /*!
     * \return Vertical dispersion
     */
    double getVerticalDispersion(void);

    //! Get the energy spread
    /*!
     * \return Energy spread
     */
    double getEnergySpread(void);

    //! Get the state of the task
    /*!
     * \return State
     */
    Tango::DevState getState(void);

    //! Start/stop watching the injection event
    /*!
     * \param state True to start watching injection events
     */
    void activateInjectionTriggering(bool state);

    //! Possible message ids
    enum MsgId {
        NEW_OPTICS  = acu::msg::id::USER + 1,  //< New optics event
        NEW_FWHM    = acu::msg::id::USER + 2,  //< New FWHM data received
        INJECTION   = acu::msg::id::USER + 3,  //< Injection in progress
    };

private:
    //! State of the periodic emittance calculation
    std::atomic<Tango::DevState> emittanceState_;

    //! State of the per frame emittance task
    std::atomic<Tango::DevState> perFrameState_;

    //! Typedef for the emittance direction
    enum EmittanceDir
    {
        HORIZONTAL,  //< Horizontal emittance
        VERTICAL,    //< Vertical emittance
    };

    //! Parent emittance device
    Tango::DeviceImpl *emittanceDevice_;

    //! Reference to the BPB CCD device
    Tango::DeviceProxy &bpmDevice_;

    //! Reference to the beam optics device
    Tango::DeviceProxy &opticsDevice_;

    //! Reference to the injection device
    Tango::DeviceProxy &injectionDevice_;

    //! Structure that contains parameter of the optical setup
    EmittanceSetup eSetup_;

    //! Name of the needed attribute in the optics device
    std::string opticsName_;

    //! Intermediate for error calculation
    double beta2M_;

    //! Intermediate for error calculation
    double coeffB_;

    //! Intermediate for error calculation
    double coeffC_;

    //! Intermediate for error calculation
    double coeffD_;

    //! Parameters from the optics device
    std::atomic<Optics> optics_;

    //! True if the task should watch injection event
    std::atomic<bool> injectionTriggering_;

    //! True if injection is in progress
    Tango::DevBoolean injectionInProgress_;

    //! Horizontal emittance
    std::atomic<EmittanceValue> xEmittance_;

    //! Vertical emittance
    std::atomic<EmittanceValue> zEmittance_;

    //! Handle received messages
    /*!
     * \param msg Message
     */
    void handle_message(acu::msg::MessagePtr msg) override;

    //! Read the parameters from the optics device
    void readOptics(void);

    //! Calculate the emittance
    /*!
     * \param dir Emittance direction to calculate
     * \param fwhm FWHM from which the emittance will be calculated
     * \return The emittance and the associated parameters
     */
    EmittanceValue calculateEmittance(EmittanceDir dir, double fwhm);

    //! Push the new emittance event
    /*!
     * \param hEm Horizontal emittance
     * \param vEm Vertical emittance
     */
    void pushEmittanceEvent(const EmittanceValue &hEm,
                            const EmittanceValue &vEm);

    //! Push an archive event for both X and Z emittances
    /*!
     * \param quality Quality of the archived data
     * \param hEm Horizontal emittance
     * \param vEm Vertical emittance
     */
    void pushEmittanceArchiveEvent(Tango::AttrQuality quality,
                                   const EmittanceValue &hEm,
                                   const EmittanceValue &vEm);
};

} /* namespace */

#endif /* EMITTANCE_TASK */


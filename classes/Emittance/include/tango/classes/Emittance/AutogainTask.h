#ifndef AUTOGAIN_TASK
#define AUTOGAIN_TASK

#include <tango/tango.h>
#include <acu/tango/device/task.h>
#include <acu/threading/message.h>

#include <chrono>


namespace Emittance_ns
{

class AutogainTask: public acu::tango::dev::Task
{
public:
    //! Constructor
    /*!
     * \param device Device that owns the task (used for logging)
     * \param ccdDevice CCD device
     * \param minIntensity Minimun image intensity
     * \param maxIntensity Maximum image intensity
     */
    AutogainTask(Tango::DeviceImpl *device, Tango::DeviceProxy &ccdDevice,
                 Tango::DeviceProxy &bpmDevice, int minIntensity,
                 int maxIntensity);

    //! Start the autogain control loop
    /*!
     * \param period Period of the control loop in milliseconds
     */
    void start(int period);

    //! Stop the autogain control loop
    void stop(void);

    //! Get the control loop state
    Tango::DevState getState(void);

    //! Set the minimum intensity bellow which the autogain is triggered
    /*!
     * \param minIntensity Minimum intensity
     */
    void setMinIntensity(int minIntensity);

    //! Set the maximum intensity above which the autogain is triggered
    /*!
     * \param maxIntensity Maximum intensity
     */
    void setMaxIntensity(int maxIntensity);

    //! Possible message ids
    enum MsgId {
        SET_MIN_INTENSITY = acu::msg::id::USER + 1,  //< Set the min intensity
        SET_MAX_INTENSITY = acu::msg::id::USER + 2,  //< Set the max intensity
        GET_STATE         = acu::msg::id::USER + 3,  //< Get the ctrl loop state
        NEW_MAX_EVENT     = acu::msg::id::USER + 4,  //< New max from BPM CCD
    };

private:
    //! Tolerance between two gain value that are considered equal
    static const double GAIN_EPSILON;

    //! Tolerance between two exposure value that are considered equal
    static const double EXPOSURE_EPSILON;

    //! Minimum gain allowed by the CCD
    static const double MIN_GAIN;

    //! Maximun gain allowed by the CCD
    static const double MAX_GAIN;

    //! Minimum exposure time allowed by the CCD (in ms)
    static const double MIN_EXPOSURE;

    //! Maximum exposure time allowed by the CCD (in ms)
    static const double MAX_EXPOSURE;

    //! Proxy to the CCD device
    Tango::DeviceProxy &ccdDevice_;

    //! Proxy to the BPM CCD device
    Tango::DeviceProxy &bpmDevice_;

    //! Maximum allowed exposure value
    double maxExposure_;

    //! Minimum allowed image intensity
    int minIntensity_;

    //! Maximum allowed image intensity
    int maxIntensity_;

    //! State of the autogain control loop
    Tango::DevState state_;

    //! Current image intensity
    Tango::DevLong currentImageIntensity_;

    //! Current image counter on BPM
    Tango::DevLong currentBPMImageCounter_;

    //! Image counter value on CCD during the last gain/exposure adjustment
    Tango::DevLong lastCCDImageCounter_;

    //! Current gain value
    Tango::DevDouble currentGain_;

    //! Current exposure value
    Tango::DevDouble currentExposure_;

    //! Handle messages received by the task
    /*!
     * \param msg Received message
     */
    void handle_message(acu::msg::MessagePtr msg) override;

    //! Check the image parameters to see if gain/exposure adjustement is needed
    void checkImage(void);

    //! Do the autogain correction
    /*!
     * This method does the work of correcting the gain and exposure of the CCD.
     */
    void doAutogain(void);

    //! Get the intensity from a new image
    void getImageIntensity(void);

    //! Set the exposure
    /*!
     * \param exposure Exposure value
     */
    void setExposure(void);

    //! Set the gain
    /*!
     * \param gain Gain value
     */
    void setGain(void);

    //! Adjust the exposure to match the average intensity
    void adjustExposure(void);

    //! Adjust the gain to match the average intensity
    void adjustGain(void);

    //! Get the image counter from CCD
    void getCCDImageCounter(void);
};

} /* namespace */

#endif /* AUTOGAIN_TASK */


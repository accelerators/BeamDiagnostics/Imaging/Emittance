# USER (TANGO CLASS) CONFIGURATION
# Here update local configuration of ${DEVICE_CLASS_TARGET}
target_sources(${DEVICE_CLASS_TARGET}
   PRIVATE
       src/AutogainTask.cpp
       src/EmittanceTask.cpp
)

target_link_libraries(${DEVICE_CLASS_TARGET}
    PRIVATE
        acu::acu
        atomic
)


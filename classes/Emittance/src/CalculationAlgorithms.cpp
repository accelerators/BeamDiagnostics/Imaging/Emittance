#include <tango/classes/Emittance/CalculationAlgorithms.h>
#include <iostream>
#include <sstream>
 
namespace Emittance_ns
{

EmittanceCalculationBase::EmittanceCalculationBase(Emittance *_emittance) : emittance(_emittance)
{
}

/**********************************************************
 *
 *		 	Emittance calculation base. Default behaviour
 * for no overriden methods.
 *
 **********************************************************/
double EmittanceCalculationBase::calculateSigmaXSource(Tango::Attribute &attr)
{
/*
* This calculus only exists for Pinhole Camera. Here is the implementation
* of the default behaviour. It's overriden in PinholeCamera class.
*/
    Tango::Except::throw_exception(
        "Invalid Request",
        "The Sigma X does not exist for this acquisition device.",
        __PRETTY_FUNCTION__);

    return 0; // avoid compiler warning.
}

double EmittanceCalculationBase::calculateSigmaZSource(Tango::Attribute &attr)
{
/*
* This calculus only exists for Pinhole Camera. Here is the implementation
* of the default behaviour. It's overriden in PinholeCamera class.
*/
    Tango::Except::throw_exception(
        "Invalid Request",
        "The Sigma Z does not exist for this acquisition device.",
        __PRETTY_FUNCTION__);

    return 0; // avoid compiler warning.
}

double EmittanceCalculationBase::calculateSigmaZSourceCorr(Tango::Attribute &attr)
{
/*
* This calculus only exists for Pinhole Camera. Here is the implementation
* of the default behaviour. It's overriden in PinholeCamera class.
*/
    Tango::Except::throw_exception(
        "Invalid Request",
        "The SigmacorZ does not exist for this acquisition device.",
        __PRETTY_FUNCTION__);

    return 0; // avoid compiler warning.
}


/**********************************************************
 *
 *		 	Pinhole camera Emittance calculation.
 *
 **********************************************************/
PinholeCameraCalculation::PinholeCameraCalculation(Emittance *_emittance) : EmittanceCalculationBase( _emittance )
{
    calculationName = "Pinhole Camera";
}

tuple<double,double,double> PinholeCameraCalculation::calculateXEmittance(double fwhm)
{
    vector<double> local_optics;

    {
        std::unique_lock<std::mutex> guard(emittance->the_mutex);
        local_optics = emittance->optics_data;
    }

    double beta_h = local_optics[2];
    double disp_h = local_optics[6];
    double e_spread = local_optics[10];

    double sigmaXpix = fwhm / FWHM_TO_SIGMA;  						//horizontal sigma beam size in pixels
    double sigmaXm = sigmaXpix * emittance->pixel_size/1e6;	//horizontal sigma beam size in meters

    double val = (sigmaXm * sigmaXm) - (emittance->detector_psf * emittance->detector_psf) - (emittance->pinhole_psf * emittance->pinhole_psf);
    double SigmaXsource,abs_err,rel_err;

    if (val <= 0)
    {
        SigmaXsource = numeric_limits<double>::quiet_NaN();
        abs_err = numeric_limits<double>::quiet_NaN();
        rel_err = numeric_limits<double>::quiet_NaN();
    }
    else
        SigmaXsource = sqrt(val) / emittance->mag ;
    sigmaXSource_um = SigmaXsource * 1e6;

    double calc_emittance = ((SigmaXsource * SigmaXsource ) - (disp_h * disp_h * e_spread * e_spread)) / beta_h;
    double emit_meter = calc_emittance;
    calc_emittance	= calc_emittance * 1e9;		// horizontal emittance value in nanometers

// Now the error computation

    if (val > 0)
    {
        BeamData bd;
        bd.beta = beta_h;
        bd.disp = disp_h;
        bd.e_spread = e_spread;
        bd.sigma = sigmaXm;
        bd.emit = emit_meter;

        computeError(bd,&abs_err,&rel_err);
    }

    return make_tuple(calc_emittance,abs_err * 1e9,rel_err * 1e2);
}

tuple<double,double,double> PinholeCameraCalculation::calculateZEmittance(double fwhm)
{
    vector<double> local_optics;

    {
        std::unique_lock<std::mutex> guard(emittance->the_mutex);
        local_optics = emittance->optics_data;
    }

    double beta_v = local_optics[3];
    double disp_v = local_optics[8];
    double e_spread = local_optics[10];

    double sigmaZpix = fwhm / FWHM_TO_SIGMA;  					//vertical sigma beam size in pixels
    double sigmaZm = sigmaZpix * emittance->pixel_size/1e6;		//vertical sigma beam size in meters

    double val = (sigmaZm*sigmaZm) - (emittance->detector_psf * emittance->detector_psf) - (emittance->pinhole_psf * emittance->pinhole_psf);
    double SigmaZsource, abs_err,rel_err;

    if (val <= 0.0)
    {
        SigmaZsource = numeric_limits<double>::quiet_NaN();
        abs_err = numeric_limits<double>::quiet_NaN();
        rel_err = numeric_limits<double>::quiet_NaN();
    }
    else
        SigmaZsource = sqrt(val) / emittance->mag;
        
    sigmaZSource_um = SigmaZsource * 1e6;

    double calc_emittance = ((SigmaZsource * SigmaZsource ) - (disp_v * disp_v * e_spread * e_spread)) / beta_v;
    double emit_meter = calc_emittance;
    calc_emittance	= calc_emittance * 1e9;		// vertical emittance value in nanometers

// Now the error computation

    if (val > 0)
    {
        BeamData bd;
        bd.beta = beta_v;
        bd.disp = disp_v;
        bd.e_spread = e_spread;
        bd.sigma = sigmaZm;
        bd.emit = emit_meter;

        computeError(bd,&abs_err,&rel_err);
    }

    return make_tuple(calc_emittance,abs_err * 1e9,rel_err * 1e2);
}

double PinholeCameraCalculation::calculateSigmaXSource(Tango::Attribute &attr)
{
    return sigmaXSource_um;
}

double PinholeCameraCalculation::calculateSigmaZSource(Tango::Attribute &attr)
{
    return sigmaZSource_um;
}


double PinholeCameraCalculation::calculateSigmaZSourceCorr(Tango::Attribute &attr)
{
    return sigmaZSourceCorr_um;
}

void PinholeCameraCalculation::computeError(BeamData &_bd,double *_abs,double *_rel)
{
    double betaM2 = _bd.beta * emittance->mag * emittance->mag;
    double A = (_bd.sigma * _bd.sigma) / betaM2;
    double B = (emittance->pinhole_psf * emittance->pinhole_psf) / betaM2;
    double C = (emittance->detector_psf * emittance->detector_psf) / betaM2;
    double D = ((_bd.disp * _bd.disp) * (_bd.e_spread * _bd.e_spread)) / _bd.beta;

    double rel_bss = emittance->absoluteErrorBeamSizeScreen / _bd.sigma;
    double deltaA_square = (A * A) * (emittance->beta_2M + (2 * (rel_bss * rel_bss)));

    double deltaB_square = B * B * emittance->coeff_B;;
    double deltaC_square = C * C * emittance->coeff_C;;
    double deltaD_square = D * D * emittance->coeff_D;;

    /*
    TANGO_LOG << "Error computation" << endl;
    TANGO_LOG << "\tDeltaA_square = " << deltaA_square << " - A = " << A << endl;
    TANGO_LOG << "\tDeltaB_square = " << deltaB_square << " - B = " << B << endl;
    TANGO_LOG << "\tDeltaC_square = " << deltaC_square << " - C = " << C << endl;
    TANGO_LOG << "\tDeltaD_square = " << deltaD_square << " - D = " << D << endl;
    */
   
    *_abs = sqrt(deltaA_square + deltaB_square + deltaC_square + deltaD_square);
    *_rel = *_abs / _bd.emit;
}

/*bool PinholeCameraCalculation::refreshFreeProperties()
{
    emittance->alphax_fp = 0;
    emittance->alphaz_fp = 0;
    emittance->betax_fp  = 0;
    emittance->betaz_fp  = 0;
    emittance->deltax_fp = 0;
    emittance->deltaz_fp = 0;
    emittance->dist_fp 	 = 0;
    emittance->etax_fp 	 = 0;
    emittance->etapx_fp  = 0;
    emittance->g_fp = 0;
    emittance->sigmaex_fp = 0;

// read the parameters

    Tango::DbData	pinhole_prop;
    pinhole_prop.push_back(Tango::DbDatum("alphax"));
    pinhole_prop.push_back(Tango::DbDatum("alphaz"));
    pinhole_prop.push_back(Tango::DbDatum("betax"));
    pinhole_prop.push_back(Tango::DbDatum("betaz"));
    pinhole_prop.push_back(Tango::DbDatum("deltax"));
    pinhole_prop.push_back(Tango::DbDatum("deltaz"));
    pinhole_prop.push_back(Tango::DbDatum("dist"));
    pinhole_prop.push_back(Tango::DbDatum("etax"));
    pinhole_prop.push_back(Tango::DbDatum("etapx"));
    pinhole_prop.push_back(Tango::DbDatum("g"));
    pinhole_prop.push_back(Tango::DbDatum("sigmaex"));

    Tango::Util *tg = Tango::Util::instance();
//    tg->get_database()->get_property(emittance->lattice_property_list, pinhole_prop);

    if (pinhole_prop[0].is_empty()==false)	pinhole_prop[0]  >> emittance->alphax_fp;
    if (pinhole_prop[1].is_empty()==false)	pinhole_prop[1]  >> emittance->alphaz_fp;
    if (pinhole_prop[2].is_empty()==false)	pinhole_prop[2]  >> emittance->betax_fp;
    if (pinhole_prop[3].is_empty()==false)	pinhole_prop[3]  >> emittance->betaz_fp;
    if (pinhole_prop[4].is_empty()==false)	pinhole_prop[4]  >> emittance->deltax_fp;
    if (pinhole_prop[5].is_empty()==false)	pinhole_prop[5]  >> emittance->deltaz_fp;
    if (pinhole_prop[6].is_empty()==false)	pinhole_prop[6]  >> emittance->dist_fp;
    if (pinhole_prop[7].is_empty()==false)	pinhole_prop[7]  >> emittance->etax_fp;
    if (pinhole_prop[8].is_empty()==false)	pinhole_prop[8]  >> emittance->etapx_fp;
    if (pinhole_prop[9].is_empty()==false)	pinhole_prop[9]  >> emittance->g_fp;
    if (pinhole_prop[10].is_empty()==false)pinhole_prop[10]  >> emittance->sigmaex_fp;

// This formula is the correct calculation!
//gcx    = g * dist / (dist - deltax);
// It was replaced to get the same values as the old analog system with the
// simplified formula gcx = g!!!!!!!!!!!

    emittance->magnification_corr_x = emittance->g_fp;
    emittance->betadx_fp = emittance->betax_fp - 2.0*emittance->deltax_fp*emittance->alphax_fp + (emittance->deltax_fp*emittance->deltax_fp) *
             (1.0 + (emittance->alphax_fp*emittance->alphax_fp)) / emittance->betax_fp;


    emittance->magnification_corr_z = emittance->g_fp * emittance->dist_fp / (emittance->dist_fp - emittance->deltaz_fp);
    emittance->betadz_fp = emittance->betaz_fp - 2.0*emittance->deltaz_fp*emittance->alphaz_fp + (emittance->deltaz_fp*emittance->deltaz_fp) *
             (1.0 + (emittance->alphaz_fp*emittance->alphaz_fp)) / emittance->betaz_fp;

    return true;
}*/


} // Emittance_ns

#include <tango/classes/Emittance/AutogainTask.h>

#include <cmath>
#include <string>
#include <vector>


namespace Emittance_ns
{

const double AutogainTask::GAIN_EPSILON = 0.01;
const double AutogainTask::EXPOSURE_EPSILON = 0.3;
const double AutogainTask::MIN_GAIN = 0.0;
const double AutogainTask::MAX_GAIN = 47.0;
const double AutogainTask::MIN_EXPOSURE = 0.018;
const double AutogainTask::MAX_EXPOSURE = 11.0 * 1000.0;

AutogainTask::AutogainTask(Tango::DeviceImpl *device,
                           Tango::DeviceProxy &ccdDevice,
                           Tango::DeviceProxy &bpmDevice, int minIntensity,
                           int maxIntensity)
    : acu::tango::dev::Task(device)
    , ccdDevice_(ccdDevice)
    , bpmDevice_(bpmDevice)
    , maxExposure_(MAX_EXPOSURE)
    , minIntensity_(minIntensity)
    , maxIntensity_(maxIntensity)
    , state_(Tango::OFF)
    , currentImageIntensity_(0)
    , currentBPMImageCounter_(0)
    , lastCCDImageCounter_(0)
    , currentGain_(0)
    , currentExposure_(0)
{
}

void AutogainTask::start(int period)
{
    auto cfg = configuration();
    cfg.enable_periodic_msg = true;
    cfg.periodic_msg_period = std::chrono::milliseconds(period);
    configure(cfg);
    state_ = Tango::ON;
    DEBUG_STREAM << "Autogain control loop started" << std::endl;
}

void AutogainTask::stop(void)
{
    unsubscribe_events();
    acu::msg::Handler::Configuration cfg = configuration();
    cfg.enable_periodic_msg = false;
    configure(cfg);
    state_ = Tango::OFF;
    DEBUG_STREAM << "Autogain control loop stoped" << std::endl;
}

Tango::DevState AutogainTask::getState(void)
{
    auto msg = acu::msg::Message::allocate(GET_STATE);
    if ((!wait(msg)) || (!msg->has_data()))
    {
        ERROR_STREAM << "Unable to read the autotgain control loop state"
                     << std::endl;
        return Tango::FAULT;
    }
    return msg->get_data<Tango::DevState>();
}

void AutogainTask::setMinIntensity(int minIntensity)
{
    auto msg = acu::msg::Message::allocate(SET_MIN_INTENSITY);
    msg->attach_data<int>(minIntensity);
    post(msg);
}

void AutogainTask::setMaxIntensity(int maxIntensity)
{
    auto msg = acu::msg::Message::allocate(SET_MAX_INTENSITY);
    msg->attach_data<int>(maxIntensity);
    post(msg);
}

void AutogainTask::handle_message(acu::msg::MessagePtr msg)
{
    switch(msg->identifier())
    {
        case acu::msg::id::PERIODIC:
            try
            {
                checkImage();
                state_ = Tango::ON;
            }
            catch (const std::exception &e)
            {
                ERROR_STREAM << "Unable to adjust the gain/exposure: "
                             << e.what()
                             << std::endl;
                state_ = Tango::ALARM;
            }
            break;
        case SET_MIN_INTENSITY:
            minIntensity_ = msg->get_data<int>();
            break;
        case SET_MAX_INTENSITY:
            maxIntensity_ = msg->get_data<int>();
            break;
        case GET_STATE:
            msg->attach_data(state_);
            break;
        case NEW_MAX_EVENT:
            try
            {
                getImageIntensity();
                // be sure to skip 2 images on CCD since the last set
                if (currentBPMImageCounter_ < lastCCDImageCounter_ + 2)
                {
                    INFO_STREAM << "Skip an image from BPM CCD before "
                                << "adjustment"
                                << std::endl;
                    break;
                }
                doAutogain();
                getCCDImageCounter();
                state_ = Tango::ON;
            }
            catch (const std::exception &e)
            {
                ERROR_STREAM << "Unable to adjust the gain/exposure: "
                             << e.what()
                             << std::endl;
                state_ = Tango::ALARM;

                // stop events and start periodic msg
                unsubscribe_events();
                auto cfg = configuration();
                cfg.enable_periodic_msg = true;
                configure(cfg);
            }
            break;
    }
}

void AutogainTask::checkImage(void)
{
    // extract attributes from bpm
    getImageIntensity();

    // extract attributes from ccd
    Tango::DevDouble framerate;
    std::vector<std::string> ccdNames{"Gain", "Exposure", "Framerate"};
    std::vector<Tango::DeviceAttribute> *ccdAttr;
    ccdAttr = ccdDevice_.read_attributes(ccdNames);
    (*ccdAttr)[0] >> currentGain_;
    (*ccdAttr)[1] >> currentExposure_;
    (*ccdAttr)[2] >> framerate;
    delete ccdAttr;

    // update max exposure from actual framerate
    maxExposure_ = std::min(MAX_EXPOSURE, 1 / framerate * 1000.0);

    INFO_STREAM << "Current max pixel value: " << currentImageIntensity_ << ", "
                << "gain: "                    << currentGain_           << ", "
                << "exposure: "                << currentExposure_
                << std::endl;

    // check if it needs correction
    if ((currentImageIntensity_ > maxIntensity_)
        || (currentImageIntensity_ < minIntensity_)
        || ((currentGain_ > MIN_GAIN)
            && (currentExposure_ < (maxExposure_ - EXPOSURE_EPSILON))))
    {
        // remove periodic msg
        auto cfg = configuration();
        cfg.enable_periodic_msg = false;
        configure(cfg);

        // subscribe to image
        acu::tango::dev::Task::EventSubscriptionForm esf;
        esf.evt_sub_form.dev_name = bpmDevice_.name();
        esf.evt_sub_form.attr_name = "MaxPixelValue";
        esf.evt_sub_form.evt_type = Tango::DATA_READY_EVENT;
        esf.evt_msg_conf.msg_id = NEW_MAX_EVENT;
        esf.evt_msg_conf.msg_post_tmo = std::chrono::seconds(1);
        subscribe_event(esf);
    }
}

void AutogainTask::doAutogain(void)
{
    // minimize gain if possible
    if ((currentGain_ > (MIN_GAIN + GAIN_EPSILON))
        && (currentExposure_ < (maxExposure_ - EXPOSURE_EPSILON)))
    {
        currentGain_ = MIN_GAIN;
        setGain();
        getCCDImageCounter();
    }

    // intensity is too high
    else if (currentImageIntensity_ > maxIntensity_)
    {
        INFO_STREAM << "Image intensity is too high: "
                    << currentImageIntensity_ << " instead of " << maxIntensity_
                    << std::endl;

        if (currentGain_ > (MIN_GAIN + GAIN_EPSILON))
        {
            if (currentImageIntensity_ == 255)
                currentGain_ = MIN_GAIN;
            else
                adjustGain();
            setGain();
        }
        else
        {
            if (currentImageIntensity_ == 255)
                currentExposure_ = 0.5 * currentExposure_;
            else
                adjustExposure();
            setExposure();
        }
    }

    // intensity is too low
    else if (currentImageIntensity_ < minIntensity_)
    {
        INFO_STREAM << "Image intensity is too low: "
                    << currentImageIntensity_ << " instead of " << minIntensity_
                    << std::endl;

        if (currentExposure_ < (maxExposure_ - EXPOSURE_EPSILON))
        {
            adjustExposure();
            setExposure();
        }
        else
        {
            adjustGain();
            setGain();
        }
    }

    // image is fine
    else
    {
        // unsubscribe to image
        unsubscribe_events();

        // enable periodic msg
        auto cfg = configuration();
        cfg.enable_periodic_msg = true;
        configure(cfg);
    }
}

void AutogainTask::getImageIntensity(void)
{
    std::vector<std::string> names{"MaxPixelValue", "ImageCounter"};
    std::vector<Tango::DeviceAttribute> *attrs;
    attrs = bpmDevice_.read_attributes(names);
    (*attrs)[0] >> currentImageIntensity_;
    (*attrs)[1] >> currentBPMImageCounter_;
    delete attrs;
}

void AutogainTask::setExposure(void)
{
    Tango::DeviceAttribute attr("Exposure", currentExposure_);
    ccdDevice_.write_attribute(attr);
    INFO_STREAM << "Set new exposure value: "
                << currentExposure_
                << std::endl;
}

void AutogainTask::setGain(void)
{
    Tango::DeviceAttribute attr("gain", currentGain_);
    ccdDevice_.write_attribute(attr);
    INFO_STREAM << "Set new gain value: "
                << currentGain_
                << std::endl;
}

void AutogainTask::adjustExposure(void)
{
    double delta = ((minIntensity_ + maxIntensity_) / 2.0)
                   / currentImageIntensity_;
    double exposure = delta * currentExposure_;
    if (exposure > maxExposure_)
        exposure = maxExposure_;
    if (exposure < MIN_EXPOSURE)
        exposure = MIN_EXPOSURE;
    currentExposure_ = exposure;
}

void AutogainTask::adjustGain(void)
{
    double delta = ((minIntensity_ + maxIntensity_) / 2.0)
                   / currentImageIntensity_;
    double gain = currentGain_ + 10 * std::log(delta);
    if (gain > MAX_GAIN)
        gain = MAX_GAIN;
    if (gain < MIN_GAIN)
        gain = MIN_GAIN;
    currentGain_ = gain;
}

void AutogainTask::getCCDImageCounter(void)
{
    Tango::DeviceAttribute counter;
    counter = ccdDevice_.read_attribute("ImageCounter");
    counter >> lastCCDImageCounter_;
}

} /* namespace */


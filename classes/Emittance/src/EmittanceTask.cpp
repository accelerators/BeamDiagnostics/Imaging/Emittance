#include <tango/classes/Emittance/EmittanceTask.h>

#include <tango/classes/Emittance/Emittance.h>

#include <limits>
#include <tuple>

#include <sys/time.h>


namespace Emittance_ns
{

EmittanceTask::EmittanceTask(Tango::DeviceImpl *device,
                             Tango::DeviceProxy &bpmDevice,
                             Tango::DeviceProxy &opticsDevice,
                             Tango::DeviceProxy &injectionDevice,
                             const EmittanceSetup &eSetup)
    : acu::tango::dev::Task(device)
    , emittanceDevice_(device)
    , bpmDevice_(bpmDevice)
    , opticsDevice_(opticsDevice)
    , injectionDevice_(injectionDevice)
    , eSetup_(eSetup)
    , injectionTriggering_(false)
    , injectionInProgress_(false)
{
    std::string deviceName = device->get_name();
    std::string cellNum = deviceName.substr(deviceName.size() - 2);
    opticsName_ = "Pinhole_" + cellNum;

    beta2M_ = (eSetup_.relErrBeta * eSetup_.relErrBeta)
              + (2 * (eSetup_.relErrMagnification
                      * eSetup_.relErrMagnification));

    double tmpB = eSetup_.absErrPinholePSF / eSetup_.pinholePSF;
    coeffB_ = beta2M_ + (2 * (tmpB * tmpB));

    double tmpC = eSetup_.absErrDetectorPSF / eSetup_.detectorPSF;
    coeffC_ = beta2M_ + (2 * (tmpC * tmpC));

    double tmpD1 = eSetup_.relErrBeta * eSetup_.relErrBeta;
    double tmpD2 = eSetup_.relErrDispersion * eSetup_.relErrDispersion;
    double tmpD3 = eSetup_.relErrEspread * eSetup_.relErrEspread;
    coeffD_ = tmpD1 + (2 * tmpD2) + (2 * tmpD3);

    // subscribe to events
    acu::tango::dev::Task::EventSubscriptionForms subForms;

    acu::tango::dev::Task::EventSubscriptionForm esfFwhm;
    esfFwhm.evt_sub_form.dev_name = bpmDevice_.name();
    esfFwhm.evt_sub_form.attr_name = "PerFrameXYFwhm";
    esfFwhm.evt_sub_form.evt_type = Tango::CHANGE_EVENT;
    esfFwhm.evt_msg_conf.msg_id = NEW_FWHM;
    esfFwhm.evt_msg_conf.msg_post_tmo = std::chrono::seconds(1);
    subForms.push_back(esfFwhm);

    acu::tango::dev::Task::EventSubscriptionForm esfOptics;
    esfOptics.evt_sub_form.dev_name = opticsDevice_.name();
    esfOptics.evt_sub_form.attr_name = opticsName_;
    esfOptics.evt_sub_form.evt_type = Tango::DATA_READY_EVENT;
    esfOptics.evt_msg_conf.msg_id = NEW_OPTICS;
    esfOptics.evt_msg_conf.msg_post_tmo = std::chrono::seconds(1);
    subForms.push_back(esfOptics);

    subscribe_events(subForms);

    // periodic task to get the emittance
    acu::msg::Handler::Configuration cfg;
    cfg.enable_periodic_msg = true;
    cfg.periodic_msg_period = std::chrono::milliseconds(1000);
    configure(cfg);

    emittanceState_.store(Tango::DevState::OFF, std::memory_order_release);
    perFrameState_.store(Tango::DevState::OFF, std::memory_order_release);
}

EmittanceValue EmittanceTask::getHorizontalEmittance(void)
{
    return xEmittance_.load(std::memory_order_acquire);
}

EmittanceValue EmittanceTask::getVerticalEmittance(void)
{
    return zEmittance_.load(std::memory_order_acquire);
}

double EmittanceTask::getHorizontalBeta(void)
{
    auto optics = optics_.load(std::memory_order_acquire);
    return optics.xBeta;
}

double EmittanceTask::getVerticalBeta(void)
{
    auto optics = optics_.load(std::memory_order_acquire);
    return optics.zBeta;
}

double EmittanceTask::getHorizontalDispersion(void)
{
    auto optics = optics_.load(std::memory_order_acquire);
    return optics.xDispersion;
}

double EmittanceTask::getVerticalDispersion(void)
{
    auto optics = optics_.load(std::memory_order_acquire);
    return optics.zDispersion;
}

double EmittanceTask::getEnergySpread(void)
{
    auto optics = optics_.load(std::memory_order_acquire);
    return optics.eSpread;
}

Tango::DevState EmittanceTask::getState(void)
{
    Tango::DevState emittanceState =
        emittanceState_.load(std::memory_order_acquire);
    Tango::DevState perFrameState =
        perFrameState_.load(std::memory_order_acquire);

    if ((emittanceState == Tango::DevState::ALARM)
        || (perFrameState == Tango::DevState::ALARM))
        return Tango::DevState::ALARM;

    if ((emittanceState == Tango::DevState::FAULT)
        || (perFrameState == Tango::DevState::FAULT))
        return Tango::DevState::FAULT;

    if ((emittanceState == Tango::DevState::OFF)
        || (perFrameState == Tango::DevState::OFF))
        return Tango::DevState::OFF;

    return Tango::DevState::ON;
}

void EmittanceTask::activateInjectionTriggering(bool state)
{
    injectionTriggering_.store(state);
    if (state)
    {
        acu::tango::dev::Task::EventSubscriptionForm esfInjection;
        esfInjection.evt_sub_form.dev_name = injectionDevice_.name();
        esfInjection.evt_sub_form.attr_name = "TopUpInjectionInProgress";
        esfInjection.evt_sub_form.evt_type = Tango::CHANGE_EVENT;
        esfInjection.evt_msg_conf.msg_id = INJECTION;
        esfInjection.evt_msg_conf.msg_post_tmo = std::chrono::seconds(1);
        subscribe_event(esfInjection);
    }
    else
    {
        try
        {
            unsubscribe_event(injectionDevice_.name(),
                              "TopUpInjectionInProgress",
                              Tango::CHANGE_EVENT);
        }
        catch (...)
        {
            INFO_STREAM << "Trying to unsubscribe to TopUpInjectionInProgress "
                           "event before having subscribed to it"
                        << std::endl;
        }
    }
}

void EmittanceTask::handle_message(acu::msg::MessagePtr msg)
{
    switch(msg->identifier())
    {
        case acu::msg::id::PERIODIC:
        {
            // read bpm ccd FWHM
            double xFwhm;
            double yFwhm;
            try
            {
                Tango::DeviceAttribute x = bpmDevice_.read_attribute("XFwhm");
                Tango::DeviceAttribute y = bpmDevice_.read_attribute("YFwhm");
                if ((x.get_quality() != Tango::ATTR_INVALID)
                    && (y.get_quality() != Tango::ATTR_INVALID))
                {
                    x >> xFwhm;
                    y >> yFwhm;
                }
                else
                {
                    xEmittance_.store(EmittanceValue());
                    zEmittance_.store(EmittanceValue());
                    INFO_STREAM << "Invalid FWHM received from BPM CCD. "
                                   "Probably because there is no beam."
                                 << std::endl;
                    break;
                }

            }
            catch (Tango::DevFailed &e)
            {
                ERROR_STREAM << "Failed to read FWHM from BPM CCD: " << e
                             << std::endl;
                emittanceState_.store(Tango::DevState::ALARM,
                                      std::memory_order_release);
                break;
            }

            // calculate emittance
            EmittanceValue hEm;
            EmittanceValue vEm;
            try
            {
                hEm = calculateEmittance(EmittanceDir::HORIZONTAL, xFwhm);
                vEm = calculateEmittance(EmittanceDir::VERTICAL, yFwhm);
            }
            catch (Tango::DevFailed &e)
            {
                ERROR_STREAM << "Error while calculating the emittance values: "
                             << e << std::endl;
                emittanceState_.store(Tango::DevState::ALARM,
                                      std::memory_order_release);
                break;
            }
            catch (std::exception &e)
            {
                ERROR_STREAM << "Error while calculating the emittance values: "
                             << e.what() << std::endl;
                emittanceState_.store(Tango::DevState::ALARM,
                                      std::memory_order_release);
                break;
            }

            xEmittance_.store(hEm, std::memory_order_release);
            zEmittance_.store(vEm, std::memory_order_release);

            // push events
            if ((!injectionTriggering_.load()) ||
                (injectionTriggering_.load() && !injectionInProgress_))
            {
                try
                {
                    Tango::AttrQuality quality;
                    if (std::isnan(hEm.emittance) || std::isnan(vEm.emittance))
                        quality = Tango::ATTR_INVALID;
                    else
                        quality = Tango::ATTR_VALID;
                    pushEmittanceArchiveEvent(quality, hEm, vEm);
                }
                catch (Tango::DevFailed &e)
                {
                    ERROR_STREAM << "Unable to push the emittance archive "
                                    "event: " << e << std::endl;
                    emittanceState_.store(Tango::DevState::ALARM,
                                          std::memory_order_release);
                    break;
                }
            }

            emittanceState_.store(Tango::DevState::ON,
                                  std::memory_order_release);
            break;
        }
        case INJECTION:
        {
            auto ed = msg->get_data<acu::tango::evt::EventData>();
            if (ed.has_error || !ed.attr_value || ed.attr_value->is_empty())
            {
                ERROR_STREAM << "Error in injection event" << std::endl;
                break;
            }
            if (ed.attr_value->get_quality() != Tango::ATTR_VALID)
            {
                ERROR_STREAM << "Invalid data in injection event" << std::endl;
                break;
            }
            *(ed.attr_value) >> injectionInProgress_;
            break;
        }
        case NEW_OPTICS:
        {
            auto ed = msg->get_data<acu::tango::evt::EventData>();
            if (ed.has_error)
            {
                ERROR_STREAM << "Error in optics event" << std::endl;
                break;
            }
            try
            {
                readOptics();
            }
            catch(Tango::DevFailed &e)
            {
                ERROR_STREAM << "Error while reading the new optics data: "
                             << e << std::endl;
                break;
            }
            break;
        }
        case NEW_FWHM:
        {
            auto ed = msg->get_data<acu::tango::evt::EventData>();
            if (ed.has_error || !ed.attr_value || ed.attr_value->is_empty())
            {
                ERROR_STREAM << "New FWHM event received but failed to read "
                                "its content" << std::endl;
                perFrameState_.store(Tango::DevState::ALARM,
                                     std::memory_order_release);
                break;
            }
            std::vector<double> fwhm;
            *(ed.attr_value) >> fwhm;
            if (fwhm.size() != 2)
            {
                ERROR_STREAM << "Malformed data in FWHM event"
                             << std::endl;
                perFrameState_.store(Tango::DevState::ALARM,
                                     std::memory_order_release);
                break;
            }

            // calculate emittance
            EmittanceValue hEm;
            EmittanceValue vEm;
            try
            {
                hEm = calculateEmittance(EmittanceDir::HORIZONTAL, fwhm[0]);
                vEm = calculateEmittance(EmittanceDir::VERTICAL, fwhm[1]);
            }
            catch (Tango::DevFailed &e)
            {
                ERROR_STREAM << "Error while calculating the emittance values: "
                             << e << std::endl;
                perFrameState_.store(Tango::DevState::ALARM,
                                     std::memory_order_release);
                break;
            }
            catch (std::exception &e)
            {
                ERROR_STREAM << "Error while calculating the emittance values: "
                             << e.what() << std::endl;
                perFrameState_.store(Tango::DevState::ALARM,
                                     std::memory_order_release);
                break;
            }

            // push events
            try
            {
                pushEmittanceEvent(hEm, vEm);
            }
            catch (Tango::DevFailed &e)
            {
                ERROR_STREAM << "Unable to push the new emittance event: "
                             << e << std::endl;
                perFrameState_.store(Tango::DevState::ALARM,
                                     std::memory_order_release);
                break;
            }
            if (injectionTriggering_.load() && injectionInProgress_)
            {
                try
                {
                    pushEmittanceArchiveEvent(ed.attr_value->get_quality(), hEm,
                                              vEm);
                }
                catch (Tango::DevFailed &e)
                {
                    ERROR_STREAM << "Unable to push the emittance archive "
                                    "event: " << e << std::endl;
                    perFrameState_.store(Tango::DevState::ALARM,
                                 std::memory_order_release);
                    break;
                }
            }
            perFrameState_.store(Tango::DevState::ON,
                                 std::memory_order_release);
            break;
        }
    }
}

void EmittanceTask::readOptics(void)
{
    Tango::DeviceAttribute attr = opticsDevice_.read_attribute(opticsName_);
    std::vector<Tango::DevDouble> opticsData;
    attr >> opticsData;

    if (opticsData.size() <= 10)
        throw std::length_error("Optics data are not formatted correctly");

    Optics optics;
    optics.xBeta = opticsData[2];
    optics.zBeta = opticsData[3];
    optics.xDispersion = opticsData[6];
    optics.zDispersion = opticsData[8];
    optics.eSpread = opticsData[10];
    optics.initialized = true;

    optics_.store(optics, std::memory_order_release);
}

EmittanceValue EmittanceTask::calculateEmittance(EmittanceDir dir, double fwhm)
{
    Optics optics = optics_.load(std::memory_order_acquire);
    if (!optics.initialized)
        readOptics();

    double beta, disp;
    if (dir == VERTICAL)
    {
        beta = optics.zBeta;
        disp = optics.zDispersion;
    }
    else
    {
        beta = optics.xBeta;
        disp = optics.xDispersion;
    }
    double e_spread = optics.eSpread;

    double sigma = fwhm / FWHM_TO_SIGMA * eSetup_.pixelSize / 1e6;

    double val = (sigma * sigma)
                 - (eSetup_.detectorPSF * eSetup_.detectorPSF)
                 - (eSetup_.pinholePSF * eSetup_.pinholePSF);

    double sigmaSource;
    if (val > 0)
        sigmaSource = std::sqrt(val) / eSetup_.magnification;
    else
        sigmaSource = numeric_limits<double>::quiet_NaN();

    double emittance = ((sigmaSource * sigmaSource)
                        - (disp * disp * e_spread * e_spread)) / beta;

    // compute errors
    double betaM2 = beta * eSetup_.magnification * eSetup_.magnification;
    double A = (sigma * sigma) / betaM2;
    double B = (eSetup_.pinholePSF * eSetup_.pinholePSF) / betaM2;
    double C = (eSetup_.detectorPSF * eSetup_.detectorPSF) / betaM2;
    double D = ((disp * disp) * (e_spread * e_spread)) / beta;

    double relErrSigma = eSetup_.absErrScreen / sigma;
    double coeffA = beta2M_ + 2 * (relErrSigma * relErrSigma);

    double deltaAsquare = A * A * coeffA;
    double deltaBsquare = B * B * coeffB_;
    double deltaCsquare = C * C * coeffC_;
    double deltaDsquare = D * D * coeffD_;

    double e = sqrt(deltaAsquare + deltaBsquare + deltaCsquare + deltaDsquare);

    // store values
    EmittanceValue em;
    em.emittance = emittance * 1e9;
    em.sigma = sigmaSource * 1e6;
    em.absoluteError = e * 1e9;
    em.relativeError = e / emittance * 1e2;

    return em;
}

void EmittanceTask::pushEmittanceEvent(const EmittanceValue &hEm,
                                       const EmittanceValue &vEm)
{
    double emittance[2] = {hEm.emittance, vEm.emittance};
    emittanceDevice_->push_change_event("PerFrameEmittance", emittance, 2, 0,
                                        false);
}

void EmittanceTask::pushEmittanceArchiveEvent(Tango::AttrQuality quality,
                                              const EmittanceValue &hEm,
                                              const EmittanceValue &vEm)
{
    double hEmValue = hEm.emittance;
    double vEmValue = vEm.emittance;
    struct timeval now;
    gettimeofday(&now, nullptr);
    emittanceDevice_->push_archive_event("Emittance_H", &hEmValue,
                                         now, quality, 1, 0, false);
    emittanceDevice_->push_archive_event("Emittance_V", &vEmValue,
                                         now, quality, 1, 0, false);
}

} /* namespace */

